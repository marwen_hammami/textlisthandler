package com.marouen.textHandler.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import com.marouen.textHandler.model.Text;
import com.marouen.textHandler.services.TextService;

/**
 * A class that will take care of client requests and use service classes to perform database specific operations and then return the view pages.
 * 
 * @author Marouen Hammami
 * @version 1.0
 */

@Controller
public class HomeController {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private TextService textService; //Service which will do all data retrieval/manipulation work

	/**
	 * A method to add a new post based on the user input.
	 *
	 * @param textÍ
	 * @param ucBuilder
	 *
	 */
	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	public ResponseEntity<Void> createPost(@RequestBody final Text text, final UriComponentsBuilder ucBuilder) {
		slf4jLogger.info("Creating Post " + text.getContent());
		if (textService.isTextExists(text)) {
			slf4jLogger.error("A Text with name " + text.getContent() + " already exist");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		textService.saveText(text);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/post/{id}").buildAndExpand(text.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/**
	 * A method to delete a certain post by id.
	 *
	 * @param id
	 *
	 */
	@RequestMapping(value = "/posts/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Text> deleteText(@PathVariable("id") final long id) {
		slf4jLogger.info("Fetching & Deleting Text with id " + id);
		Text post = textService.findById(id);
		if (post == null) {
			slf4jLogger.error("Unable to delete. Text with id " + id + " not found");
			return new ResponseEntity<Text>(HttpStatus.NOT_FOUND);
		}
		textService.deleteById(id);
		return new ResponseEntity<Text>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/edit")
	public String getEditPage() {
		return "/partials/editPost";
	}

	@RequestMapping(value = "/mainPage")
	public String getMainPage() {
		return "/partials/mainPage";
	}

	/**
	 * A method to get a single post by id.
	 *
	 * @param id
	 * @return Text object requested.
	 */
	@RequestMapping(value = "/posts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Text> getPost(@PathVariable("id") final long id) {
		slf4jLogger.info("Fetching Text with id " + id);
		Text text = textService.findById(id);
		if (text == null) {
			slf4jLogger.error("Text with id " + id + " not found");
			return new ResponseEntity<Text>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Text>(text, HttpStatus.OK);
	}

	/**
	 * A method to retrieve all the posts
	 *
	 * @return list of all the posts
	 */
	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	public ResponseEntity<List<Text>> listAllTexts() {
		List<Text> texts = textService.findAllTexts();
		if (texts.isEmpty()) {
			return new ResponseEntity<List<Text>>(HttpStatus.NO_CONTENT);//or it could be HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Text>>(texts, HttpStatus.OK);
	}

	@RequestMapping(value = "/")
	public ModelAndView mainView(final HttpServletResponse response) throws IOException {
		return new ModelAndView("home");
	}

	/**
	 * A method to update a certain post by id.
	 *
	 * @param id
	 * @param text
	 * @return the Text object modified.
	 */
	@RequestMapping(value = "/posts/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Text> updateText(@PathVariable("id") final long id, @RequestBody final Text text) {
		slf4jLogger.info("Updating Text " + id);
		Text currentPost = textService.findById(id);
		if (currentPost == null) {
			slf4jLogger.error("Text with id " + id + " not found");
			return new ResponseEntity<Text>(HttpStatus.NOT_FOUND);
		}
		currentPost.setContent(text.getContent());
		currentPost.setDate(text.getDate());
		textService.updateText(text);
		return new ResponseEntity<Text>(currentPost, HttpStatus.OK);
	}
}
