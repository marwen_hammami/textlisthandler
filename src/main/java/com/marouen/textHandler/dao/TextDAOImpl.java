package com.marouen.textHandler.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.marouen.textHandler.model.Text;

/**
 * Hibernate specific DAO implementation.
 * 
 * @author Marouen Hammami
 * @version 1.0
 */
@Repository("textDAO")
@Transactional
public class TextDAOImpl implements TextDAO {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(TextDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void deleteById(final long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Text p = session.load(Text.class, id);
		if (null != p) {
			session.delete(p);
		}
		slf4jLogger.info("Post deleted successfully, post details =" + p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Text> findAllTexts() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Text> postsList = session.createQuery("from Text").list();
		for (Text p : postsList) {
			slf4jLogger.info("Posts List::" + p);
		}
		return postsList;
	}

	@Override
	public Text findByContent(final String content) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Text where content=:content");
		query.setParameter("content", content);
		Text p = (Text) query.uniqueResult();
		slf4jLogger.info("Post loaded successfully, post details=" + p);
		return p;
	}

	@Override
	public Text findById(final long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Text p = session.load(Text.class, id);
		slf4jLogger.info("Post loaded successfully, post details=" + p);
		return p;
	}

	@Override
	public boolean isTextExists(final Text text) {
		return findByContent(text.getContent()) != null;
	}

	@Override
	public void saveText(final Text text) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(text);
		slf4jLogger.info("Post saved successfully, post details= " + text);
	}

	@Override
	public void updateText(final Text text) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(text);
		slf4jLogger.info("Post updated successfully, post details= " + text);
	}

}
