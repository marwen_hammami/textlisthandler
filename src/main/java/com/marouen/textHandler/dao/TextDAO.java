package com.marouen.textHandler.dao;

import java.util.List;

import com.marouen.textHandler.model.Text;

/**
 * Interface to declare the methods that we will use in our project.
 *
 * @author Marouen Hammami
 * @version 1.0
 */
public interface TextDAO {

	/**
	 * Delete a post by ID.
	 *
	 * @param id
	 */
	public void deleteById(long id);

	/**
	 * Return all the posts
	 *
	 * @return list
	 */
	public List<Text> findAllTexts();

	/**
	 * Find a post by its content.
	 *
	 * @param content
	 * @return text object.
	 */
	public Text findByContent(String content);

	/**
	 * Find a post by ID
	 *
	 * @param id
	 * @return text object
	 */
	public Text findById(long id);

	/**
	 * Check if a post exists.
	 *
	 * @param text
	 * @return true or false.
	 */
	public boolean isTextExists(Text text);

	/**
	 * Save the post.
	 *
	 * @param text
	 */
	public void saveText(Text text);

	/**
	 * Modify a post content.
	 *
	 * @param text
	 */
	public void updateText(Text text);
}
