package com.marouen.textHandler.config;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Marouen Hammami
 * @version 1.0
 */
@Configuration
@ComponentScan(basePackages = {"com.marouen.textHandler.services", "com.marouen.textHandler.dao"})
@EnableTransactionManagement
public class RootContextConfiguration {

	private static final Logger slf4jLogger = LoggerFactory.getLogger(RootContextConfiguration.class);

	@Bean
	public DataSource dataSource() {
		// Use HSQL embedded database
		return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).build();

	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hibernate.format_sql", "true");
		return properties;
	}

	@Bean
	public HibernateTemplate getHibernateTemplate(final SessionFactory sessionFactory) {
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		return hibernateTemplate;
	}

	@Bean
	public SessionFactory getSessionFactory() {
		LocalSessionFactoryBean lsfb = new LocalSessionFactoryBean();
		try {
			lsfb.setDataSource(dataSource());
			lsfb.setHibernateProperties(getHibernateProperties());
			lsfb.setPackagesToScan(new String[] {"com.marouen.textHandler"});
			lsfb.afterPropertiesSet();
		} catch (IOException e) {
			slf4jLogger.error("Exception : " + e);
		}
		return lsfb.getObject();
	}

	@Bean
	public HibernateTransactionManager transactionManager(final SessionFactory sessionFactory) {
		HibernateTransactionManager htm = new HibernateTransactionManager();
		htm.setSessionFactory(sessionFactory);
		return htm;
	}
}
