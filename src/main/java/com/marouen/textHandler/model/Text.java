package com.marouen.textHandler.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 *
 * @author Marouen Hammami
 * @version 1.0
 */
@Entity
@Table(name = "postsTable")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Text {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "content")
	private String content;

	@Column(name = "date")
	private Date date;

	public Text() {

	}

	public Text(final long id, final String content) {
		this.id = id;
		this.content = content;
		this.date = new Date();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Text)) {
			return false;
		}
		Text other = (Text) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

	public String getContent() {
		return content;
	}

	public Date getDate() {
		return date;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + (int) (id ^ (id >>> 32));
		return result;
	}

	public void setContent(final String content) {
		this.content = content;
	}

	public void setDate(final Date date) {
		this.date = date;
	}

	public void setId(final long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Text [id=" + id + ", content=" + content + ", added in " + date + "]";
	}

}
