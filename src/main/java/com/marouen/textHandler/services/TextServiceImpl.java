package com.marouen.textHandler.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.marouen.textHandler.dao.TextDAO;
import com.marouen.textHandler.model.Text;

/**
 * Class implementing the service methods using DAO class.
 * 
 * @author Marouen Hammami
 * @version 1.0
 *
 */
@Service("textService")
@Transactional
public class TextServiceImpl implements TextService {

	@Autowired
	private TextDAO textDAO;

	@Override
	public void deleteById(final long id) {
		this.textDAO.deleteById(id);
	}

	@Override
	public List<Text> findAllTexts() {
		return this.textDAO.findAllTexts();
	}

	@Override
	public Text findByContent(final String content) {
		return this.textDAO.findByContent(content);
	}

	@Override
	public Text findById(final long id) {
		return this.textDAO.findById(id);
	}

	@Override
	public boolean isTextExists(final Text text) {
		return this.textDAO.isTextExists(text);
	}

	@Override
	public void saveText(final Text text) {
		this.textDAO.saveText(text);
	}

	@Override
	public void updateText(final Text text) {
		this.textDAO.updateText(text);
	}

}
