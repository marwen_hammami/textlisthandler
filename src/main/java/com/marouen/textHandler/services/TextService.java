package com.marouen.textHandler.services;

import java.util.List;

import com.marouen.textHandler.model.Text;

/**
 * Interface to declare the methods that we will use in our project using Hibernate DAO classes to work with Text Objects.
 *
 * @author Marouen Hammami
 * @version 1.0
 */
public interface TextService {

	/**
	 * Delete a post by ID.
	 *
	 * @param id
	 */
	void deleteById(long id);

	/**
	 * Return all the posts
	 *
	 * @return list
	 */
	List<Text> findAllTexts();

	/**
	 * Find a post by its content.
	 *
	 * @param content
	 * @return text object.
	 */
	Text findByContent(String content);

	/**
	 * Find a post by ID
	 *
	 * @param id
	 * @return text object
	 */
	Text findById(long id);

	/**
	 * Check if a post exists.
	 *
	 * @param text
	 * @return true or false.
	 */
	public boolean isTextExists(Text text);

	/**
	 * Save the post.
	 *
	 * @param text
	 */
	void saveText(Text text);

	/**
	 * Modify a post content.
	 *
	 * @param text
	 */
	void updateText(Text text);
}
