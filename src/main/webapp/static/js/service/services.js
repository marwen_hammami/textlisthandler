'use strict';

App.constant("baseURL", "http://localhost:8080/TextListHandler/")
    .service('appFactory', ['$resource', 'baseURL', function ($resource, baseURL) {

        this.getPosts = function () {

            return $resource(baseURL + "posts/:id", null, {
                'update': {
                    method: 'PUT'
                }
            });

        };

        this.savePost = function () {

            return $resource(baseURL + "posts/", null, {
                'save': {
                    method: 'POST'
                }
            });
        };

        this.deletePost = function () {
            return $resource(baseURL + "posts/:id", null, {
                'del': {
                    method: 'DELETE'
                }
            });

        }

}]);