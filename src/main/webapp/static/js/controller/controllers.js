'use strict';

App.controller('AppController', ['$scope', 'appFactory', function ($scope, appFactory) {

    $scope.showPosts = false;

    $scope.items = appFactory.getPosts().query(
        function (response) {
            $scope.items = response;
            $scope.showPosts = true;

        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });

    $scope.submitPost = function () {
        $scope.newPost = {
            content: "",
            date: ""

        };
        console.log($scope.newItem);
        $scope.newPost.content = $scope.newItem;
        $scope.newPost.date = new Date().toISOString();
        appFactory.savePost().save($scope.newPost).$promise.then(
            function (response) {
                $scope.items = appFactory.getPosts().query();
                $scope.showPosts = true;
            },
            function (response) {
            	$scope.showPosts = false;
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }

        );

        $scope.newItem = "";
    };


    $scope.deleteItem = function (item) {
        $scope.item = item;
        appFactory.deletePost().del({
            id: parseInt($scope.item.id, 10)
        }).$promise.then(
            function (response) {
                $scope.items = appFactory.getPosts().query();
            }

        );

    };
}])

.controller('EditController', ['$scope', '$stateParams', '$location', 'appFactory', function ($scope, $stateParams, $location, appFactory) {
    $scope.message = "Loading...";
    $scope.showPosts = false;
    $scope.item = appFactory.getPosts().get({
        id: parseInt($stateParams.id, 10)
    }, null).$promise.then(
        function (response) {
            $scope.item = response;
            $scope.showEdit = true;
        },
        function (response) {
            $scope.message = "Error: " + response.status + " " + response.statusText;
        });


    $scope.updatePost = function (item) {
        $scope.item = item;
        console.log($scope.item.id);
        appFactory.getPosts().update({
            id: $scope.item.id,
        }, $scope.item).$promise.then(
            function (response) {
                $location.url('/mainPage');
            }
        );
    };

}]);