'use strict';

var App = angular.module('myApp', ['ui.router', 'ngResource']);

App.config(function ($stateProvider, $urlRouterProvider) {

    
    $stateProvider

    // route for the home page
        .state('app', {
        url: '/',
        views: {
            'content': {
                templateUrl: CONTEXT_ROOT + '/mainPage',
                controller: 'AppController'
            }
        }
    })

    // route for the edit page
    .state('app.edit', {
        url: 'edit/:id',
        views: {
            'content@': {
                templateUrl: CONTEXT_ROOT + '/edit',
                controller: 'EditController'
            }
        }
    })


    $urlRouterProvider.otherwise('/');
});