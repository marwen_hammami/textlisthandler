<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head 
         content must come *after* these tags -->

    <link rel="stylesheet" href=" <c:url value='./libs/bootstrap/dist/css/bootstrap.min.css' />" />

</head>

<body ng-app="myApp">

    <div ui-view="content"></div>


    <!-- build:js scripts/main.js -->
    <script type="text/javascript" src="<c:url value='./libs/angular/angular.min.js' />"></script>
    <script type="text/javascript" src="<c:url value='./libs/angular-resource/angular-resource.min.js' />"></script>
    <script src="<c:url value='./libs/angular-ui-router/release/angular-ui-router.min.js' />"></script>

    <script>
        var CONTEXT_ROOT = '${pageContext.request.contextPath}';
    </script>
    <script type="text/javascript" src="<c:url value='./static/js/app.js' /> "></script>
    <script type="text/javascript" src="<c:url value='./static/js/service/services.js' /> "></script>
    <script type="text/javascript" src="<c:url value='./static/js/controller/controllers.js' /> "></script>
    <!-- endbuild -->
</body>

</html>