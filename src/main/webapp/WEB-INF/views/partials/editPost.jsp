<div class="container" ng-controller="EditController">
    <div class="page-header">
        <h1>Edit Text</h1>
    </div>
    <div class="col-xs-12" ng-if="!showEdit">
        <h3>{{message}}</h3>
    </div>
    <form class="form-horizontal" ng-submit="updatePost(item)" ng-if="showEdit">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="title">
            </div>
        </div>
        <div class="form-group">
            <label for="content" class="col-sm-2 control-label">Content</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="12" ng-model=item.content ng-required=true>

                </textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Post</button>
            </div>
        </div>

    </form>

</div>