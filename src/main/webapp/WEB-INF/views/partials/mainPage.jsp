<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <div class="container" ng-controller="AppController">
        <div class="page-header">
            <h1>A Post List</h1>
        </div>
        <div class="col-xs-12" ng-if="!showPosts">
            <h3>{{message}}</h3>
        </div>
        <div ng-show="showPosts">
            <form>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>

                        <input type="text" class="form-control" placeholder="Search..." ng-model="searchText">

                    </div>
                </div>
            </form>
            <div class="alert alert-info" role="alert" ng-hide="items && items.length > 0">There are no items yet.</div>
            <form class="form-horizontal" role="form" ng-submit="submitPost()">
                <div class="form-group" ng-repeat="item in items | orderBy:-item.date| filter:searchText">
                    <div class="col-xs-9">
                        <label>
                            <a ui-sref="app.edit({id:item.id})"> {{item.content}}
                               </a>
                        </label>
                    </div>
                    <div class="col-xs-3">
                        <button class="pull-right btn btn-danger" type="button" title="Delete" ng-click="deleteItem(item)">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </div>
                </div>
                <hr />
                <div class="input-group">
                    <input type="text" class="form-control" ng-model="newItem" placeholder="Enter the description..." />
                    <span class="input-group-btn">
					<button class="btn btn-default" type="submit"
						ng-disabled="!newItem" title="Add" >
						<span class="glyphicon glyphicon-plus"></span>
                    </button>
                    </span>
                </div>
            </form>
        </div>
    </div>