### **Spring MVC/Angular JS restful web services Show case** ###

Demonstrates the capabilities of the Spring MVC web framework as REST API producer, Angular JS/Bootsrap as frontend and HSQLDB as embedded databse.

## Requirements ##
* JDK 8

Oracle Java 8 is required, go to Oracle Java website to download it and install into your system.

Optionally, you can set JAVA_HOME environment variable and add <JDK installation dir>/bin in your PATH environment variable.

* Apache Maven

Download the latest Apache Maven from http://maven.apache.org, and uncompress it into your local system.

Optionally, you can set M2_HOME environment varible, and also do not forget to append <Maven Installation dir>/bin your PATH environment variable.

## Run this project##
* Clone the repository

```
#!git

    git clone https://marwen_hammami@bitbucket.org/marwen_hammami/textlisthandler.git
```
* And enter the root folder, run ``` mvn tomcat7:run``` to start up an embedded tomcat7 to serve this application.
* Go to http://localhost:8080/TextListHandler/ to test it.

or

In your preferred IDE such as SpringSource Tool Suite (STS) or IDEA:

* Import spring-mvc-showcase as a Maven Project
* Drag-n-drop the project onto the "SpringSource tc Server Developer Edition" or another Servlet 2.5 or > Server to run, such as Tomcat.

# Spring Boot #
If you are interested in Spring Boot, please let me know and I'll upload a Boot version.